Akeed Projet  <!-- omit in toc -->
====

> Bilel Jegham <contact.bileljegham@gmail.com>

Ce dépôt contient un squelette pour un projet *OracleXE* et *ADF*.

- [Database](#database)
  - [Composition](#composition)
  - [Utilisation](#utilisation)
  - [Test](#test)
- [Application](#application)
  - [Composition](#composition-1)
  - [Test](#test-1)
- [Docker](#docker)
  - [Installation](#installation)
  - [Utilisation](#utilisation-1)



## Database

Le dossier database contient l'ensemble des codes et des tests liés à la base de données. Le versionnement de la base de données est géré via l'outil [Liquibase](https://www.liquibase.org/).


### Composition
Le dossier **src** contient les scripts SQL de lancement de la base.
Et le dossier **test** contient les fichiers `feature` des tests.


### Utilisation
Afin de mettre à jour votre base de données de données vous pouvez utiliser la commande suivante :
```sh
# Windows
start ./bin/liquibase.bat --driver=oracle.jdbc.OracleDriver \
     --url="jdbc:oracle:thin:@<url>" \
     --username=<user> \
     --password=<pass> \
     --changeLogFile=data.yml \
     --logLevel=sql \
     update
# Linux
sh ./bin/liquibase --driver=oracle.jdbc.OracleDriver \
     --url="jdbc:oracle:thin:@<url>" \
     --username=<user> \
     --password=<pass> \
     --changeLogFile=data.yml \
     --logLevel=sql \
     update
```
Pour ajouter une modification à la base de données de données, vous pouvez créer un fichier sql dans le dossier `database/src`. 
Pour le format du fichier sql devra respecter les recommandations : https://www.liquibase.org/documentation/sql_format.html

Le nom de fichier devra être sous la forme `<num-chanset>-xxx.sql`

### Test
Processus de Test

!["Processus de Test"](https://www.plantuml.com/plantuml/img/LP0v3i9034Lxd-AB4eaS8Oe83L8A5d40Cp48aMI6YOS6k3icGIoDjsyzVnb5hnmrjGt1ykd0JZ6PPWTXJL4cu1xl6lyR_QKPLCj_oaQ2ZuhINoNoSIJ4SKQeMNdFlM0nnuqGs9rj1qb3Onp2W-1y6tmddQ7Av5AxbxZJLRLTLmFA9ILyBCtozy3YgnaWBqUpUsut29sl1RiHNrPB-gcHfP3btzi_Yf56ouH2kjg-ymG0)

Les fichiers `.feature` sont utilisés par le projet [Cucumber UtPLSQL](https://github.com/BilelJegham/cumcumber-utplsql) pour générer des fichiers `.sql` contenant des procédures UTPLSQL. Celles-ci peuvent être intégrées dans une base de données où UtPLSQL est préalablement installée.

Enfin nous utilisons l'interface de ligne de commande [UtPLSQL-cli](https://github.com/utPLSQL/utPLSQL-cli) pour lancer les tests et obtenir le rapport de couverture.


## Application

Sur le même modéle que le dossier `database`.

### Composition
Le dossier **src** contient le projet ADF et le dossier **test** contient les fichiers `feature` des tests.

### Test
Processus de Test

!["Processus de Test"](https://www.plantuml.com/plantuml/img/JKvB2iD02Drx2icoa1wX17g3zWJ6cXAQ-M1qLNhtJ2OVkX2Vlezsiv6QXmbA6mTYm-Po7OJCLHga6NUCVuA7vAHMMQqG8HD_w2tub4dYw06_y0DE8QSeiLZkpXvwqOElPdQ3Cx-zrRdK2WyTd85bkHs3Jk9h_NW1)

Les fichiers `.feature` sont utilisés par le projet [Cucumber Selenium](https://github.com/firashajbi/cucumber_selenium) pour exécuter des instructions Selenium et générer par la suite un rapport.

## Docker
### Installation
 1. Oracle 18.3-small :    
 Pour générer l'image docker 18.3, il faut télécharger le fichier *db_home.zip*
(Oracle Database 18c Standard Edition 2 for Linux x64) disponible sur http://www.oracle.com/technetwork/database/enterprise-edition/downloads/index.html

**Il est fortement conseillé de push cette image sur un *Docker Container Repository*.**    
 L'image étant lourde (20.4GB). Il sera plus pratique pour les développeurs de l'utiliser.

### Utilisation

```shell
# Lancement
docker-compose up 

# Arret
docker-compose down

```




