#!/bin/bash

set -ev

cd ~/utPLSQL/source

sqlplus -S -L / AS SYSDBA  <<SQL
@install_headless.sql c##utplsql hr
exit
SQL

