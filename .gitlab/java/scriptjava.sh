curl -Lk -o utplsqlcli.zip  https://github.com/utPLSQL/utPLSQL-cli/releases/download/v3.1.7/utPLSQL-cli.zip
unzip utplsqlcli.zip
cp .gitlab/java/ojdbc8.jar utPLSQL-cli/lib/ojdbc8.jar
sh .gitlab/java/run_test.sh