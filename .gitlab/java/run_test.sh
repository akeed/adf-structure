#!/bin/bash

set -ev

utPLSQL-cli/bin/utplsql run c##utplsql/hr@0.0.0.0:1521/ORCLCDB \
-f=ut_documentation_reporter  -c \
-f=ut_coverage_sonar_reporter -o=coverage.xml \
-f=ut_coverage_html_reporter -o=coverage.html \
-f=ut_sonar_test_reporter     -o=test_results.xml \
--failure-exit-code=0


# #!/bin/bash

# set -ev

# utPLSQL-cli/bin/utplsql run c##hr/hr@0.0.0.0:1521/ORCLCDB \
# -f=ut_documentation_reporter  -c \
# -d \
# -s -f=ut_coverage_html_reporter -o=coverage.html \
# --failure-exit-code=0
